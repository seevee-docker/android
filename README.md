# Android SDK (Alpine)

This image provides Android SDK with Oracle Java. Includes gradle binaries.

# Supported versions:
Java: 8
Android SDK: 28.0.1
Gradle: 4.9
