FROM openjdk:8-jdk-alpine

LABEL authors="Can Kutlu Kinay <me@ckk.im>, Chris Vincent <seevee@pm.me>"

ENV SDK_VERSION=4333796 \
    TOOLS_VERSION=28.0.1 \
    SDK_CHECKSUM="92ffee5a1d98d856634e8b71132e8a95d96c83a63fde1099be3d86df3106def9" \
    ANDROID_HOME=/opt/android-sdk \
    LD_LIBRARY_PATH="${ANDROID_HOME}/tools/lib64/qt:${ANDROID_HOME}/tools/lib/libQt5:${LD_LIBRARY_PATH}/" \
    GRADLE_VERSION=4.10 \
    GRADLE_HOME=/opt/gradle

ENV PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/tools/bin:$ANDROID_HOME/platform-tools:$ANDROID_HOME/build-tools/$SDK_VERSION:$GRADLE_HOME/bin

# Add curl
RUN apk add --no-cache curl unzip

# Download Android SDK
RUN curl -sSLO "https://dl.google.com/android/repository/sdk-tools-linux-${SDK_VERSION}.zip" && \
    echo "${SDK_CHECKSUM}  sdk-tools-linux-${SDK_VERSION}.zip" | sha256sum -c - && \
    mkdir -p $ANDROID_HOME && \
    unzip -q "sdk-tools-linux-${SDK_VERSION}.zip" -d "${ANDROID_HOME}" && \
    rm -Rf "sdk-tools-linux-${SDK_VERSION}.zip"

# Accept license agreements
RUN echo y | sdkmanager "platform-tools" "build-tools;${TOOLS_VERSION}" > /dev/null && \
    echo y | sdkmanager "platforms;android-10" "platforms;android-15" "platforms;android-16" "platforms;android-17" "platforms;android-18" "platforms;android-19" > /dev/null && \
    echo y | sdkmanager "platforms;android-20" "platforms;android-21" "platforms;android-22" "platforms;android-23" "platforms;android-24" "platforms;android-25" "platforms;android-26" "platforms;android-27" "platforms;android-28" > /dev/null && \
    echo y | sdkmanager "extras;android;m2repository" "extras;google;google_play_services" "extras;google;instantapps" "extras;google;m2repository" > /dev/null && \
    echo y | sdkmanager "add-ons;addon-google_apis-google-15" "add-ons;addon-google_apis-google-16" "add-ons;addon-google_apis-google-17" "add-ons;addon-google_apis-google-18" "add-ons;addon-google_apis-google-19" "add-ons;addon-google_apis-google-21" "add-ons;addon-google_apis-google-22" "add-ons;addon-google_apis-google-23" "add-ons;addon-google_apis-google-24" > /dev/null

# Here be dragons
RUN mkdir -p $ANDROID_HOME/tools/keymaps \
    && touch $ANDROID_HOME/tools/keymaps/en-us \
    # Licenses taken from https://github.com/mindrunner/docker-android-sdk
    && mkdir -p $ANDROID_HOME/licenses \
    && echo -e "\n8933bad161af4178b1185d1a37fbf41ea5269c55\n" > $ANDROID_HOME/licenses/android-sdk-license \
    && echo -e "\n84831b9409646a918e30573bab4c9c91346d8abd\n" > $ANDROID_HOME/licenses/android-sdk-preview-license

# Install Gradle

ARG GRADLE_DOWNLOAD_SHA256=248cfd92104ce12c5431ddb8309cf713fe58de8e330c63176543320022f59f18
RUN set -o errexit -o nounset \
    && echo "Installing build dependencies" \
    && apk add --no-cache --virtual .build-deps \
    ca-certificates \
    openssl \
    unzip \
    \
    && echo "Downloading Gradle" \
    && wget -O gradle.zip "https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip" \
    \
    && echo "Checking download hash" \
    && echo "${GRADLE_DOWNLOAD_SHA256} *gradle.zip" | sha256sum -c - \
    \
    && echo "Installing Gradle" \
    && unzip gradle.zip \
    && rm gradle.zip \
    && mv "gradle-${GRADLE_VERSION}" "${GRADLE_HOME}/" \
    && ln -s "${GRADLE_HOME}/bin/gradle" /usr/bin/gradle \
    \
    && apk del .build-deps \
    \
    && echo "Adding gradle user and group" \
    && addgroup -S -g 1000 gradle \
    && adduser -D -S -G gradle -u 1000 -s /bin/ash gradle \
    && mkdir /home/gradle/.gradle \
    && chown -R gradle:gradle /home/gradle \
    \
    && echo "Symlinking root Gradle cache to gradle Gradle cache" \
    && ln -s /home/gradle/.gradle /root/.gradle
